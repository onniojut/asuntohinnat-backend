package com.oksoft.asuntohinnat;

import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class DuplicateRemoveServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp){
		
		String yearStr = req.getParameter("year");
		String quarterStr = req.getParameter("quarter");
		
		
		if(yearStr != null && quarterStr != null){
			// 2005-2012
			int year = Integer.parseInt(yearStr);
			// 0-4
			int quarter = Integer.parseInt(quarterStr);
			
			for(int apartment_type = 0; apartment_type < 6; apartment_type++){
				this.removeDuplicates(year, quarter, apartment_type);
			}
		}
		
	}
	
	public void removeDuplicates(int year, int quarter, int apart_type){

		DAO dao = new DAO();
		
		// get datapoints
		ArrayList<DataPoint> datapoints = dao.queryDataPoints(year, quarter, apart_type);
		
		// find duplicates
		ArrayList<DataPoint> duplicates = new ArrayList<DataPoint>();
		for(int indx1 = 0; indx1 < datapoints.size(); indx1++){
			DataPoint dp1 = datapoints.get(indx1);
			for(int indx2 = indx1+1; indx2 < datapoints.size(); indx2++){
				DataPoint dp2 = datapoints.get(indx2);
				if(dp1.isDuplicateWith(dp2)){
					duplicates.add(dp2);
				}
			}
		}
		
		// remove duplicates
		if(duplicates.size() > 0){
			dao.removeDatapoints(duplicates);
		}
	}
}
