package com.oksoft.asuntohinnat;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.Cursor;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;
import com.googlecode.objectify.util.DAOBase;

public class DAO extends DAOBase {
	static {
		ObjectifyService.register(DataPoint.class);
	}

	public JSONObject getTestDataPointsJSON() throws JSONException{
		JSONArray jsonArr = new JSONArray();
		int postcode = 12345;
		int age = 0;
		int type = 0;
		int price = 4000;
		int count = 20;
		for(int year = 2005; year < 2013; year++){
			for(int quarter = 0; quarter < 5; quarter++){
				
				DataPoint dp = new DataPoint(postcode, year, quarter, type, age, price, count);
				count++;
				price += count*quarter;
				
				jsonArr.put(dp.toJSON());
			}
		}
		JSONObject ret = new JSONObject();
		ret.put("result", jsonArr);
		return ret;
	}
	
	public ArrayList<DataPoint> getDataPoints(Cursor start, int chunkSize){
		Query<DataPoint> q = ofy().query(DataPoint.class).chunkSize(chunkSize).startCursor(start);
		ArrayList<DataPoint> ret = new ArrayList<DataPoint>();
		for(DataPoint dp : q){
			ret.add(dp);
		}
		return ret;
	}
	
	public void removeDatapoints(ArrayList<DataPoint> dps){
		ofy().delete(dps);
	}

	public void putDataPoint(DataPoint dp){
		ofy().put(dp);
	}

	public void putDataPoints(Iterable<DataPoint> dps){
		ofy().put(dps);
	}
	
	public ArrayList<DataPoint> queryDataPoints(int year, int quarter, int apartment_type){
		
		Query<DataPoint> q = ofy().query(DataPoint.class).filter("year", year);
		
		if(apartment_type >= 0 && apartment_type < 6) q.filter("apartment_type", apartment_type);
		if(quarter >= 0  && quarter < 5) q.filter("quarter", quarter);
		
		ArrayList<DataPoint> ret = new ArrayList<DataPoint>();
		for(DataPoint dp : q){
			ret.add(dp);
		}
		return ret;
	}

	public ArrayList<DataPoint> queryDataPoints(int postcode, int year, int quarter, int apartment_type,
			int apartment_age){
		
		Query<DataPoint> q = ofy().query(DataPoint.class).filter("postcode", postcode);
		
		if(apartment_age >= 0 && apartment_age < 8) q.filter("apartment_age", apartment_age);
		if(apartment_type >= 0 && apartment_type < 6) q.filter("apartment_type", apartment_type);
		if(quarter >= 0  && quarter < 5) q.filter("quarter", quarter);
		if(year >= 1000 && year < 2100) q.filter("year", year);
	
		ArrayList<DataPoint> ret = new ArrayList<DataPoint>();
		for(DataPoint dp : q){
			ret.add(dp);
		}
		return ret;
	}
	
	public JSONObject queryDataPointsJSON(int postcode, int year, int quarter, int apartment_type,
			int apartment_age) throws JSONException{
		
		Query<DataPoint> q = ofy().query(DataPoint.class).filter("postcode", postcode);
		
		if(apartment_age >= 0 && apartment_age < 8) q.filter("apartment_age", apartment_age);
		if(apartment_type >= 0 && apartment_type < 6) q.filter("apartment_type", apartment_type);
		if(quarter >= 0  && quarter < 5) q.filter("quarter", quarter);
		if(year >= 1000 && year < 2100) q.filter("year", year);
	
		JSONArray jsonArr = new JSONArray();
		for(DataPoint dp : q){
			jsonArr.put(dp.toJSON());
		}
		JSONObject ret = new JSONObject();
		ret.put("copyright", this.getCopyrightString());
		ret.put("result", jsonArr);
		return ret;
	}

	public int yearStringToInt(String year){
		return Integer.parseInt(year);
	}

	public int quarterStringToInt(String quarter){
		if(quarter.startsWith("1. nelj") || quarter.startsWith("1st quarter"))
			return 1;
		else if(quarter.startsWith("2. nelj") || quarter.startsWith("2nd quarter"))
			return 2;
		else if(quarter.startsWith("3. nelj") || quarter.startsWith("3rd quarter"))
			return 3;
		else if(quarter.startsWith("4. nelj") || quarter.startsWith("4th quarter"))
			return 4;
		else if(quarter.startsWith("Vuosi") || quarter.startsWith("Whole year"))
			return 0;
		else
			return -1;
	}

	public int apartmentTypeStringToInt(String type){
		if(type.startsWith("Kerrostalo yksi") || type.startsWith("Blocks of flats, one-room flat"))
			return 1;
		else if(type.startsWith("Kerrostalo kaksi") || type.startsWith("Blocks of flats, two-room flat"))
			return 2;
		else if(type.startsWith("Kerrostalo kolmi") || type.startsWith("Blocks of flats, three-room flat+"))
			return 3;
		else if(type.startsWith("Kerrostalot yht") || type.startsWith("Blocks of flats total"))
			return 4;
		else if(type.startsWith("Rivi- ja pientalot yht") || type.startsWith("Terraced houses total"))
			return 5;
		else if(type.startsWith("Talotyypit yht") || type.startsWith("Building types total"))
			return 0;
		else
			return -1;
	}

	public int postcodeStringToInt(String postcode){
		return Integer.parseInt(postcode);
	}

	public int apartmentAgeStringToInt(String age){
		if(age.contains("-1949"))
			return 1;
		else if(age.contains("1950-"))
			return 2;
		else if(age.contains("1960-"))
			return 3;
		else if(age.contains("1970-"))
			return 4;
		else if(age.contains("1980-"))
			return 5;
		else if(age.contains("1990-"))
			return 6;
		else if(age.contains("2000-"))
			return 7;
		else if(age.startsWith("Rakennusvuodet yht") || age.startsWith("Year of construction"))
			return 0;
		else
			return -1;
	}

	public int priceStringToInt(String price){
		int periodIndex = price.indexOf(".");
		if(periodIndex >= 0){
			return Integer.parseInt(price.substring(0, periodIndex));
		}
		else{
			return Integer.parseInt(price);
		}
		
	}

	public int countStringToInt(String count){
		int periodIndex = count.indexOf(".");
		if(periodIndex >= 0){
			return Integer.parseInt(count.substring(0, periodIndex));
		}
		else{
			return Integer.parseInt(count);
		}
	}
	
	public JSONObject getCopyrightJSON(){
		JSONObject obj = new JSONObject();
		try {
			obj.put("copyright", this.getCopyrightString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	public String getCopyrightString(){
		return "\u00A9 Tilastokeskus [22.8.2014]";
	}
}
