package com.oksoft.asuntohinnat;

import javax.persistence.Id;

import org.json.JSONException;
import org.json.JSONObject;

import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class DataPoint {

	@Id Long id;
	@Indexed int postcode;
	@Indexed int year;
	@Indexed int quarter;
	@Indexed int apartment_type;
	@Indexed int apartment_age;
	int mean_price;
	int count;
	
	@SuppressWarnings("unused")
	private DataPoint(){}
	
	public DataPoint(int postcode, int year, int quarter, int apartment_type,
			int apartment_age, int mean_price, int count){
		this.postcode = postcode;
		this.year = year;
		this.quarter = quarter;
		this.apartment_type = apartment_type;
		this.apartment_age = apartment_age;
		this.mean_price = mean_price;
		this.count = count;
	}
	
	public JSONObject toJSON() throws JSONException{
		JSONObject obj = new JSONObject();
		
		obj.put("postcode", postcode);
		obj.put("year", year);
		obj.put("quarter", quarter);
		obj.put("type", apartment_type);
		obj.put("age", apartment_age);
		obj.put("price", mean_price);
		obj.put("count", count);
		
		return obj;
	}
	
	boolean isDuplicateWith(DataPoint dp){
		if(this.postcode == dp.postcode &&
				this.year == dp.year &&
				this.quarter == dp.quarter &&
				this.apartment_age == dp.apartment_age &&
				this.apartment_type == dp.apartment_type){
			return true;
		}
		else{
			return false;
		}
	}
	
	void copyValuesOf(DataPoint dp){
		this.postcode = dp.postcode;
		this.year = dp.year;
		this.quarter = dp.quarter;
		this.apartment_type = dp.apartment_type;
		this.apartment_age = dp.apartment_age;
		this.mean_price = dp.mean_price;
		this.count = dp.count;
	}
}
