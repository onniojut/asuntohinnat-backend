package com.oksoft.asuntohinnat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;

@SuppressWarnings("serial")
public class ParseDataServlet extends HttpServlet {
	
	private static final Logger log = Logger.getLogger(ParseDataServlet.class.getName());

	public void doPost(HttpServletRequest req, HttpServletResponse res) {
        BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));
        if(blobKey != null){
        	try {
        		this.putOrUpdateAllDataPoints(blobKey);
				//this.putAllDataPoints(blobKey);
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}
	
	public void putAllDataPoints(BlobKey blobKey) throws IOException{
		DAO dao = new DAO();

		ArrayList<DataPoint> newDataPoints = this.readDataPointsFromBlob(blobKey);
		
		// batch put is more efficient
		dao.putDataPoints(newDataPoints);
		
		log.info("new datapoints written: " + newDataPoints.size());
	}
	
	public void putOrUpdateAllDataPoints(BlobKey blobKey) throws IOException{
		BlobstoreInputStream bsis = new BlobstoreInputStream(blobKey);
    	BufferedReader br = new BufferedReader(new InputStreamReader(bsis));
    	
    	DAO dao = new DAO();
    	
    	ArrayList<ArrayList<DataPoint>> dataPointArrays = new ArrayList<ArrayList<DataPoint>>();
    	for(int apartment_type_indx = 0; apartment_type_indx < 6; apartment_type_indx++){
    		dataPointArrays.add(new ArrayList<DataPoint>());
    	}
		
		// read line by line
		String year = "";
		String quarter = "";
		String apartment_type = "";
		String postcode = "";
		String apartment_age = "";
		String price = "";
		String count = "";

		String line;
		while((line = br.readLine()) != null)   {
			// parse values and create DataPoint
			String valueStrings[] = line.split(";");
			
			if(!valueStrings[0].equals("")) year = valueStrings[0];
			if(!valueStrings[1].equals("")) quarter = valueStrings[1];
			if(!valueStrings[2].equals("")) apartment_type = valueStrings[2];
			if(!valueStrings[3].equals("")) postcode = valueStrings[3];
			if(!valueStrings[4].equals("")) apartment_age = valueStrings[4];
			
			if(valueStrings.length == 7 
					&& !valueStrings[5].equals(".")
					&& !valueStrings[6].equals(".")){
				// values found
				price = valueStrings[5];
				count = valueStrings[6];

				DataPoint found = new DataPoint(dao.postcodeStringToInt(postcode), 
						dao.yearStringToInt(year), 
						dao.quarterStringToInt(quarter), 
						dao.apartmentTypeStringToInt(apartment_type),
						dao.apartmentAgeStringToInt(apartment_age),
						dao.priceStringToInt(price),
						dao.countStringToInt(count));

				// sort to 6 datapoint arrays
				ArrayList<DataPoint> newDataPoints = dataPointArrays.get(found.apartment_type);
				newDataPoints.add(found);
			}
		}
		
		bsis.close();
		br.close();
		
		int yearInt = dataPointArrays.get(0).get(0).year;
		int quarterInt = dataPointArrays.get(0).get(0).quarter;
		for(int apartment_type_indx = 0; apartment_type_indx < 6; apartment_type_indx++){
			ArrayList<DataPoint> dataPointsToBeSaved = new ArrayList<DataPoint>();
			ArrayList<DataPoint> newDatapoints = dataPointArrays.get(apartment_type_indx);
			ArrayList<DataPoint> storedDataPoints = dao.queryDataPoints(yearInt, quarterInt, apartment_type_indx);
			
			for(DataPoint newDataPoint : newDatapoints){
				boolean found = false;
				for(DataPoint storedDataPoint : storedDataPoints){
					if(newDataPoint.isDuplicateWith(storedDataPoint)){
						found = true;
						storedDataPoint.copyValuesOf(newDataPoint);
						dataPointsToBeSaved.add(storedDataPoint);
					}
				}
				if(!found){
					dataPointsToBeSaved.add(newDataPoint);
				}
			}
			
			// batch put is more efficient
			dao.putDataPoints(dataPointsToBeSaved);
		}
		
	}
	
	ArrayList<DataPoint> readDataPointsFromBlob(BlobKey blobKey) throws IOException{
		BlobstoreInputStream bsis = new BlobstoreInputStream(blobKey);
    	BufferedReader br = new BufferedReader(new InputStreamReader(bsis));
    	
    	DAO dao = new DAO();
    	
		ArrayList<DataPoint> newDataPoints = new ArrayList<DataPoint>();
		
		// read line by line
		String year = "";
		String quarter = "";
		String apartment_type = "";
		String postcode = "";
		String apartment_age = "";
		String price = "";
		String count = "";

		String line;
		while((line = br.readLine()) != null)   {
			// parse values and create DataPoint
			String valueStrings[] = line.split(";");
			
			if(!valueStrings[0].equals("")) year = valueStrings[0];
			if(!valueStrings[1].equals("")) quarter = valueStrings[1];
			if(!valueStrings[2].equals("")) apartment_type = valueStrings[2];
			if(!valueStrings[3].equals("")) postcode = valueStrings[3];
			if(!valueStrings[4].equals("")) apartment_age = valueStrings[4];
			
			if(valueStrings.length == 7 
					&& !valueStrings[5].equals(".")
					&& !valueStrings[6].equals(".")){
				// values found
				price = valueStrings[5];
				count = valueStrings[6];

				DataPoint found = new DataPoint(dao.postcodeStringToInt(postcode), 
						dao.yearStringToInt(year), 
						dao.quarterStringToInt(quarter), 
						dao.apartmentTypeStringToInt(apartment_type),
						dao.apartmentAgeStringToInt(apartment_age),
						dao.priceStringToInt(price),
						dao.countStringToInt(count));

				newDataPoints.add(found);
			}
		}
		
		bsis.close();
		br.close();
		
		return newDataPoints;
	}
}
