package com.oksoft.asuntohinnat;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

@SuppressWarnings("serial")
public class DataUploadServlet extends HttpServlet {
	
	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		String auth = req.getHeader("Authorization");
		if(auth != null && UserAuthentication.authenticate(auth)){

		}
		else{
			res.setHeader("WWW-Authenticate", "BASIC realm=\"authentication\"");
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

//		String auth = req.getHeader("Authorization");
//		if(auth != null && UserAuthentication.authenticate(auth)){
			
			Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
			List<BlobKey> blobKey = blobs.get("myFile");

			if (blobKey == null || blobKey.size() == 0) {
			    res.sendRedirect("/");
			} else {
			    // we have new blob, start parsing as task in task queue
				res.setContentType("text/html");
				res.getWriter().write("blob " + blobKey.get(0).getKeyString() + " created");
				
			    Queue queue = QueueFactory.getDefaultQueue();
			    TaskOptions opts = TaskOptions.Builder
			    		.withUrl("/parsedata")
						.method(Method.POST)
			    		.param("blob-key", blobKey.get(0).getKeyString());
			    queue.add(opts);
			}
			
//		}
//		else{
//			res.setHeader("WWW-Authenticate", "BASIC realm=\"authentication\"");
//			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//		}
	}

}
