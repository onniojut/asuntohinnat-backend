package com.oksoft.asuntohinnat;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("serial")
public class DataServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
//		String remoteUser = req.getRemoteUser();
//		Principal principal = req.getUserPrincipal();
		
		//String authType = req.getAuthType();
		String auth = req.getHeader("Authorization");
		
		if(auth != null && UserAuthentication.authenticate(auth)){

			HttpSession session = req.getSession();
//			String username = (String) session.getAttribute("username");
//			String password = (String) session.getAttribute("password");
			
			// get parameters
			String postcodeStr = req.getParameter("postcode");
			String apartment_typeStr = req.getParameter("type");
			String apartment_ageStr = req.getParameter("age");
			String yearStr = req.getParameter("year");
			String quarterStr = req.getParameter("quarter");
			
			DAO dao = new DAO();
			
			try {
				// postcode is required, apartment type and age are optional (defaults to all)
				if(postcodeStr != null){

					int postcode = Integer.parseInt(postcodeStr);
					int apartment_type = -1;
					int apartment_age = -1;
					int year = -1;
					int quarter = -1;

					if(apartment_typeStr != null) apartment_type = Integer.parseInt(apartment_typeStr);
					if(apartment_ageStr != null) apartment_age = Integer.parseInt(apartment_ageStr);
					if(apartment_typeStr != null) apartment_type = Integer.parseInt(apartment_typeStr);
					if(apartment_ageStr != null) apartment_age = Integer.parseInt(apartment_ageStr);
					if(yearStr != null) year = Integer.parseInt(yearStr);
					if(quarterStr != null) quarter = Integer.parseInt(quarterStr);

					JSONObject obj = dao.queryDataPointsJSON(postcode, year, quarter, apartment_type, apartment_age);

					resp.setContentType("application/json");
					resp.getWriter().write(obj.toString());
				}
				else{
					resp.setContentType("text/html");
					resp.getWriter().write("null");
				}
			} catch(com.google.apphosting.api.ApiProxy.OverQuotaException e){
				JSONObject errobj = new JSONObject();
				try {
					errobj.put("error", "quota exceeded");
					errobj.put("error_description", "Palvelun kapasiteetti ylittynyt. Yrit� my�hemmin uudelleen.");

					resp.setContentType("application/json");
					resp.getWriter().write(errobj.toString());
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			} catch (Exception e) {
				JSONObject errobj = new JSONObject();
				try {
					errobj.put("error", "query error");
					errobj.put("error_description", "Haussa tapahtui virhe. Yrit� uudelleen.");

					resp.setContentType("application/json");
					resp.getWriter().write(errobj.toString());
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}

		}
		else{
			resp.setHeader("WWW-Authenticate", "BASIC realm=\"authentication\"");
			resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}
		
	}
	
}
