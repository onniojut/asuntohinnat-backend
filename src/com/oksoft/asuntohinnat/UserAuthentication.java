package com.oksoft.asuntohinnat;

import java.io.IOException;

import org.apache.geronimo.mail.util.Base64;

public class UserAuthentication {

	// validate user
	static public boolean authenticate(String auth) throws IOException {
		if(auth == null)
			return false;
		
		if(!auth.toUpperCase().startsWith("BASIC "))
			return false;
		
		String userpassEncoded = auth.substring(6);
		
		byte[] userpassDecoded = Base64.decode(userpassEncoded);
		String userpassDecodedString = new String(userpassDecoded);
		
		return isValidUsernameAndPassword(userpassDecodedString);
	}
	
	static public boolean isValidUsernameAndPassword(String usernameAndPasswd){
		
		// TODO this is ugly
		if(usernameAndPasswd.equals("FLYqXouWCb:iAq8dV6ZTt")){
			return true;
		}
		else {
			return false;
		}
	}
}
